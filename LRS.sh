#!/usr/bin/env bash

start() {
    #si l'app est deja demarrée
    echo "Application starting";
    docker compose -f ./docker-compose.yml up -d;
}

stop() {
    #si l'app ne tourne pas
    echo "Stopping the application";
    docker compose -f ./docker-compose.yml stop;
}

backup_db() {
    #si l'app ne tourne pas
    echo "Backuping application database";
    docker exec -it db mysqldump -u username -p database_name > data-dump.sql;
}

restore_db() {
    echo "stop"
}

case $1 in
    start)
        start ;;
    stop)
        stop ;;
    backup_db)
        backup_db ;;
    restore_db)
        restore_ ;;
    *)
        echo "Not a valid option" ;;
esac