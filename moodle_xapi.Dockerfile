FROM docker.io/bitnami/moodle:latest

# wget installation for downloading the plugin
RUN apt-get update -qq && \
    apt-get install -qy \
    wget

#Install xapi plugin according to https://github.com/xAPI-vle/moodle-logstore_xapi/blob/master/docs/install-with-download.md
WORKDIR /tmp/

RUN wget https://github.com/xAPI-vle/moodle-logstore_xapi/archive/refs/tags/v4.7.0.tar.gz -O moodle-logstore_xapi-4.7.0.tar.gz
RUN tar xvzf moodle-logstore_xapi-4.7.0.tar.gz
#COPY ./moodle-logstore_xapi-4.7.0.tar.gz /bitnami/moodle/admin/tool/log/store/moodle-logstore_xapi-4.7.0.tar.gz

#RUN tar xvzf /bitnami/moodle/admin/tool/log/store/moodle-logstore_xapi-4.7.0.tar.gz
#RUN rm -rf /bitnami/moodle/admin/tool/log/store/moodle-logstore_xapi-4.7.0.tar.gz
RUN mv moodle-logstore_xapi-4.7.0 /bitnami/moodle/admin/tool/log/store/moodle-logstore_xapi-4.7.0 && \
    rm -rf moodle-logstore_xapi-4.7.0.tar.gz

WORKDIR /