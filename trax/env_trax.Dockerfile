FROM php:8.0-apache-buster

COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer

ADD config_files/ /tmp/

RUN chmod -R 777 /tmp && chmod -R +t /tmp \
    && apt-get update && apt-get install -y git cron zlib1g-dev postgresql sudo\
    && /tmp/trax_php_extensions.sh

RUN /tmp/trax_build.sh

EXPOSE 80

ENTRYPOINT apache2-foreground