#!/bin/bash

export $(grep -v '^#' /tmp/.env | xargs -d '\n')

echo "Cloning repo branch beta5..."
git clone --recursive -b beta5 https://github.com/trax-project/trax2-starter-lrs.git /var/www/html/trax/

chown -R www-data:www-data /var/www/html/trax/
echo "Configuring PHP..."
sed -i 's/;max_input_vars = 1000/max_input_vars = 5000/g' /usr/local/etc/php/php.ini-production
mv /usr/local/etc/php/php.ini-production /usr/local/etc/php/conf.d/php.ini

echo "Configuring trax..."
cp /var/www/html/trax/.env.example /var/www/html/trax/.env

sed -i "s/APP_NAME.*$/APP_NAME=$TRAX_APP_NAME/g" /var/www/html/trax/.env
sed -i "s/APP_ENV.*$/APP_ENV=$TRAX_APP_ENV/g" /var/www/html/trax/.env
sed -i "s/APP_URL.*$/APP_URL=$TRAX_APP_URL/g" /var/www/html/trax/.env
sed -i "s/DB_CONNECTION.*$/DB_CONNECTION=$TRAX_DB_CONNECTION/g" /var/www/html/trax/.env
sed -i "s/DB_HOST.*$/DB_HOST=$TRAX_DB_HOST/g" /var/www/html/trax/.env
sed -i "s/DB_PORT.*$/DB_PORT=$TRAX_DB_PORT/g" /var/www/html/trax/.env
sed -i "s/DB_DATABASE.*$/DB_DATABASE=$TRAX_DB_NAME/g" /var/www/html/trax/.env
sed -i "s/DB_USERNAME.*$/DB_USERNAME=$TRAX_DB_USER/g" /var/www/html/trax/.env
sed -i "s/DB_PASSWORD.*$/DB_PASSWORD=$TRAX_DB_PASS/g" /var/www/html/trax/.env
#echo "APP_SECURE=true" >> /var/www/html/trax/.env

chmod 750 /var/www/html/trax/storage/
chmod 750 /var/www/html/trax/bootstrap/cache
chown www-data:www-data /var/www/html/trax/.env

echo "Configuring apache..."
echo DocumentRoot "/var/www/html/trax/public" >> /etc/apache2/apache2.conf
a2enmod rewrite

# If Trax application preparation failed, then creating an error webpage, in order to the apache process to start.
if [ ! -d /var/www/html/trax/public ]; then
  echo "Configuring error page..."
	mkdir /var/www/html/trax
	mkdir /var/www/html/trax/public
	touch /var/www/html/trax/public/index.html
	echo "<html><body>Trax installation error</body></html>" > /var/www/html/trax/public/index.html
	chown www-data:www-data -R /var/www/html/trax
fi

cat <<EOF > /etc/apache2/conf-available/trax.conf
<VirtualHost *:80>
     ServerAdmin root@localhost
     ServerName $TRAX_DN
     DocumentRoot /var/www/html/trax/public
     <Directory /var/www/html/trax/public>
        Options FollowSymLinks
        AllowOverride All
        Order allow,deny
        allow from all
     </Directory>
     RewriteEngine on
#RewriteCond %{SERVER_NAME} =$TRAX_DN
</VirtualHost>
EOF

a2enmod rewrite
a2enconf trax.conf

### Clearing environment variables
# TRAX_APP_NAME=''
# TRAX_APP_ENV=''
# TRAX_APP_KEY=''
# TRAX_APP_DEBUG=''
# TRAX_APP_URL=''
# TRAX_ADMIN_MAIL=''
# TRAX_ADMIN_PASS=''
# TRAX_LOG_VERSION=''
# TRAX_VIDEO_VERSION=''
# TRAX_DN=''

# TRAX_DB_CONNECTION=''
# TRAX_DB_HOST=''
# TRAX_DB_PORT=''
# TRAX_DB_DATABASE=''
# TRAX_DB_USERNAME=''
# TRAX_DB_PASSWORD=''
###
