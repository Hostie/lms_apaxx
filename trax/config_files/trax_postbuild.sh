#!/bin/bash

export $(grep -v '^#' /tmp/.env | xargs -d '\n')

mkdir /var/www/.composer
chown www-data:www-data /var/www/.composer
echo "Trax install..."
su - www-data -s /bin/bash -c "cd /var/www/html/trax/ && composer update && composer install"
su - www-data -s /bin/bash -c "cd /var/www/html/trax/ && php artisan key:generate && php artisan cache:clear && php artisan view:clear && php artisan migrate && php artisan config:cache && php artisan route:cache"

echo "Creating admin user..."
su - www-data -s /bin/bash -c "cd /var/www/html/trax/ && php artisan admin:create"

# Trax insertion for moodle user
sudo touch /tmp/trax_insertion.sql
echo "INSERT INTO public.trax_owners (uuid, name, meta, created_at, updated_at)VALUES ('3256e78c-70dc-4c94-9055-f563162aff9a', 'Default Store', '[]', now(), now());" >> /tmp/trax_insertion.sql
echo "INSERT INTO public.trax_clients (name, active, meta, permissions, admin, created_at, updated_at, entity_id, owner_id)VALUES ('moodle', true, '[]', '{ \"xapi-scope.all\": true}', false, now(), now(), null, 1);" >> /tmp/trax_insertion.sql
echo "INSERT INTO public.trax_basic_http (username, password) VALUES ('moodle', '$ENDPOINT_PASSWORD');" >> /tmp/trax_insertion.sql
echo "INSERT INTO public.trax_accesses (uuid, name, cors, active, meta, permissions, admin, inherited_permissions, created_at, updated_at, credentials_id, credentials_type, client_id)VALUES ('$ENDPOINT_UUID', 'moodle', '*', true, '[]', '[]', false, true, now(), now(), 1, 'Trax\Auth\Stores\BasicHttp\BasicHttp', 1);" >> /tmp/trax_insertion.sql

PGPASSWORD=$(echo $TRAX_DB_PASS) psql -h $TRAX_DB_HOST -U $TRAX_DB_USER -d $TRAX_DB_NAME -a -f /tmp/trax_insertion.sql
sudo rm /tmp/trax_insertion.sql