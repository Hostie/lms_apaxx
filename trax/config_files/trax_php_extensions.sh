#!/usr/bin/env bash
set -e

echo "Installing apt dependencies"

BUILD_DEPENDENCIES="gettext gnupg libcurl4-openssl-dev libfreetype6-dev libicu-dev libjpeg62-turbo-dev \
  libldap2-dev libmariadbclient-dev libmemcached-dev libpng-dev libpq-dev libxml2-dev libxslt-dev \
  unixodbc-dev uuid-dev"

POSTGRES_DEPENCIES="libpq5 libpq-dev"

RUNTIME_DEPENDENCIES="ghostscript libaio1 libcurl4 libgss3 libicu63 libmcrypt-dev libxml2 libxslt1.1 \
  libzip-dev locales sassc unixodbc unzip zip libonig-dev"

MEMCACHED_DEPENDENCIES="libmemcached11 libmemcachedutil2"

apt-get update
apt-get install -y --no-install-recommends apt-transport-https \
    $BUILD_DEPENDENCIES \
    $POSTGRES_DEPENCIES \
    $RUNTIME_DEPENDENCIES \
    $MEMCACHED_DEPENDENCIES \

echo 'Generating locales..'
echo 'en_US.UTF-8 UTF-8' > /etc/locale.gen
#echo 'fr_US.UTF-8 UTF-8' > /etc/locale.gen
locale-gen

echo "Installing php extensions"

# ZIP
docker-php-ext-configure zip --with-zip
docker-php-ext-install zip

# Postgres
docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql
docker-php-ext-install pdo pdo_pgsql pgsql

docker-php-ext-install -j$(nproc) \
    intl \
    opcache \
    soap \
    xsl \
    tokenizer \
    bcmath \
    exif

# GD
docker-php-ext-configure gd --with-freetype=/usr/include/ --with-jpeg=/usr/include/
docker-php-ext-install -j$(nproc) gd

# APCu, igbinary, Memcached, MongoDB, Redis, Solr, uuid, XMLRPC (beta)
pecl install apcu igbinary memcached solr uuid xmlrpc-beta
docker-php-ext-enable apcu igbinary memcached solr uuid xmlrpc

echo 'apc.enable_cli = On' >> /usr/local/etc/php/conf.d/docker-php-ext-apcu.ini

#Clear cache and build's dependencies
pecl clear-cache
apt-get remove --purge -y $BUILD_DEPENDENCIES
apt-get autoremove -y
apt-get clean
rm -rf /var/lib/apt/lists/*
