FROM php:7.4.33-apache-bullseye

#Endpoint of image is /var/www/html

#Composer installation and accessibility
ENV COMPOSER_ALLOW_SUPERUSER=1
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer --version

#Apache requirements and configuration for traxlrs
COPY config_files/vhosts.conf /etc/apache2/sites-available/000-default.conf
COPY config_files/apache.conf /etc/apache2/conf-available/z-app.conf
COPY config_files/php.ini /usr/local/etc/php/conf.d/app.ini

RUN a2enmod rewrite
RUN /etc/init.d/apache2 restart

#Generic installation
#git for clonning repositories / gnupg for ssl / unzip and zip for compose packages.lock extraction
RUN apt-get update -qq && \
    apt-get install -qy \
    git \ 
    gnupg \
    unzip \
    zip \
    libpq-dev \
    cron 

#RUN docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
#PHP moduls requirements for traxlrs (ctype, fileinfo, json, mbstring, openssl, tokenizer, xml are by default installed)

RUN docker-php-ext-install -j$(nproc) bcmath pdo_pgsql

#Trax installation according to https://github.com/trax-project/trax2-starter-lrs
RUN git clone https://github.com/trax-project/trax2-starter-lrs traxlrs
WORKDIR traxlrs
RUN composer install

#TODO -> Update permissions 
RUN chmod -R 777 bootstrap/cache
RUN chmod -R 777 storage

COPY config_files/.env /var/www/html/traxlrs/.env

RUN php artisan key:generate
#RUN php artisan migrate
#RUN php artisan admin:create

# php artisan admin:list
# php artisan admin:update
# php artisan admin:delete
EXPOSE 80
EXPOSE 443