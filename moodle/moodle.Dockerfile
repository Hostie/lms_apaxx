#FROM php:8.0-apache-buster
# => PHP Fatal error:  Method lang_string::__set_state() must take exactly 1 argument in /var/www/html/moodle/lib/moodlelib.php on line 10611

FROM php:7.4-apache-buster
ADD /moodle/config_files/ /tmp/

RUN chmod -R 777 /tmp && chmod -R +t /tmp \
    && apt-get update && apt-get install -y git cron sudo postgresql uuid-runtime\
    && /tmp/moodle_php_extensions.sh \
    && mkdir /var/www/moodledata \
    && chown www-data /var/www/moodledata \
    && chmod -R 777 /var/www/html/

RUN /tmp/moodle_build.sh

EXPOSE 80

ENTRYPOINT apache2-foreground
