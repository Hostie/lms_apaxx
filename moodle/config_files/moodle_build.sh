#!/bin/bash

export $(grep -v '^#' /tmp/.env | xargs -d '\n')

echo "Cloning repo, branch: $MDL_VERSION ..."
git clone -b $MDL_BRANCH_NAME --single-branch git://git.moodle.org/moodle.git /var/www/html/moodle

chown -R www-data:www-data /var/www/html/moodle/
echo "Configuring PHP..."
sed -i 's/;max_input_vars = 1000/max_input_vars = 5000/g' /usr/local/etc/php/php.ini-production
sed -i 's/upload_max_filesize = 2M/upload_max_filesize = 300M/g' /usr/local/etc/php/php.ini-production
sed -i 's/post_max_size = 8M/post_max_size = 300M/g' /usr/local/etc/php/php.ini-production
mv /usr/local/etc/php/php.ini-production /usr/local/etc/php/conf.d/php.ini

### Conf apache
echo "Configuring apache..."
echo DocumentRoot "/var/www/html/moodle/" >>/etc/apache2/apache2.conf
a2enmod rewrite

cat <<'EOF' >/etc/apache2/conf-enabled/moodle.conf
<VirtualHost *:80>
     DocumentRoot /var/www/html/moodle
     <Directory /var/www/html/moodle>
        Options FollowSymLinks
        AllowOverride All
        Order allow,deny
        allow from all
     </Directory>
</VirtualHost>
EOF
##

# cat <<'EOF' >/etc/apache2/conf-enabled/moodle.conf
# <VirtualHost *:80>
#      ServerAdmin root@localhost
#      ServerName localhost
#      DocumentRoot /var/www/html/moodle
#      <Directory /var/www/html/moodle>
#         Options FollowSymLinks
#         AllowOverride All
#         Order allow,deny
#         allow from all
#      </Directory>
#      RewriteEngine on
# #RewriteCond %{SERVER_NAME} =localhost
# </VirtualHost>
# EOF

### Clearing variables
# echo "Clearing environnement variables..."
# MDL_DB_HOST=''
# MDL_DB_USER=''
# MDL_DB_PASS=''
# MDL_DB_PORT=''
# MDL_ADMIN_NAME=''
# MDL_ADMIN_PASS=''

# TRAX_DB_TYPE=''
# TRAX_DB_HOST=''
# TRAX_DB_NAME=''
# TRAX_DB_PORT=''
# TRAX_DB_USER=''
# TRAX_DB_PASS=''
###