#!/bin/bash

export $(grep -v '^#' /tmp/.env | xargs -d '\n')

if [ $MDL_VERSION = "39" ];then
  echo "Installing trax log plugin"
  if ! sudo test -d /var/www/html/moodle/admin/tool/log/store/trax/ ; then
    sudo curl -Lo /tmp/trax.zip https://github.com/trax-project/moodle-trax-logs/archive/refs/tags/v0.20.zip
    sudo unzip -q /tmp/trax.zip -d /tmp/
    sudo rm /tmp/trax.zip
    mkdir -p /var/www/html/moodle/admin/tool/log/store/trax/
    sudo mv /tmp/moodle-trax-logs-0.20 /var/www/html/moodle/admin/tool/log/store/trax/
    sudo chown -R www-data:www-data /var/www/html/moodle/admin/tool/log/store/trax/

    # Trax insertion completion
    echo "INSERT INTO public.mdl_config_plugins (plugin, name, value) VALUES ('logstore_trax', 'version', '2020060900');" >> /tmp/trax_plugin_installation.sql
    echo "INSERT INTO public.mdl_config_plugins (plugin, name, value) VALUES ('logstore_trax', 'platform_iri', 'http://moodle');" >> /tmp/trax_plugin_installation.sql
    echo "INSERT INTO public.mdl_config_plugins (plugin, name, value) VALUES ('logstore_trax', 'lrs_endpoint', 'http://trax/trax/api/$ENDPOINT_UUID/xapi/std/');" >> /tmp/trax_plugin_installation.sql
    echo "INSERT INTO public.mdl_config_plugins (plugin, name, value) VALUES ('logstore_trax', 'lrs_username', 'moodle');" >> /tmp/trax_plugin_installation.sql
    echo "INSERT INTO public.mdl_config_plugins (plugin, name, value) VALUES ('logstore_trax', 'lrs_password', '$ENDPOINT_PASSWORD');" /tmp/trax_plugin_installation.sql
  
    PGPASSWORD=$(echo $MDL_DB_PASS) psql -h $MDL_DB_HOST -U $MDL_DB_USER -d $MDL_DB_NAME -a -f /tmp/trax_plugin_installation.sql
    sudo rm /tmp/trax_plugin_installation.sql

    # Trax logs plugin activation
    PGPASSWORD=$(echo $MDL_DB_PASS) psql -h $MDL_DB_HOST -U $MDL_DB_USER -d $MDL_DB_NAME -c "update mdl_config_plugins set value ='logstore_standard,logstore_trax' where name = 'enabled_stores'; WITH config_log AS( insert into mdl_config_log (userid, timemodified, name, oldvalue, value, plugin) VALUES (2, extract(epoch from now()), 'tool_logstore_visibility', 0, 1, 'logstore_trax') RETURNING id) INSERT INTO mdl_logstore_standard_log (eventname, component, action, target, objecttable, objectid, crud, edulevel, contextid, contextlevel, contextinstanceid, userid, courseid, relateduserid, anonymous, other, timecreated, origin, ip, realuserid) VALUES ('\core\event\config_log_created', 'core', 'created', 'config_log', 'config_log', (SELECT id from config_log), 'c', '0', '1', '10', '0', '2', '0', NULL, '0', '{\"name\":\"tool_logstore_visibility\",\"oldvalue\":\"0\",\"value\":\"1\",\"plugin\":\"logstore_trax\"}', extract(epoch from now()), 'web', '127.0.0.1', NULL) ;"
  else
      echo "Trax log plugin already installed"
  fi

  echo "Installing trax video plugin"
  if sudo test ! -d /var/www/html/moodle/mod/traxvideo ; then
    sudo curl -Lo /tmp/traxvideo.zip https://github.com/trax-project/moodle-trax-video/archive/refs/tags/v0.4.zip
    sudo unzip -q /tmp/traxvideo.zip -d /var/www/html/moodle/mod/
    sudo rm /tmp/traxvideo.zip
    mkdir -p /var/www/html/moodle/mod/traxvideo
    sudo mv /var/www/html/moodle/mod/moodle-trax-video-0.4 /var/www/html/moodle/mod/traxvideo/
    sudo chown -R www-data:www-data /var/www/html/moodle/mod/traxvideo/
  else
    echo "Trax video plugin already installed"
  fi

else
  echo "No plugins to install"
fi