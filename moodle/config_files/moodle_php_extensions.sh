#!/usr/bin/env bash

set -e

export $(grep -v '^#' /tmp/.env | xargs -d '\n')

echo "Installing apt dependencies"

BUILD_DEPENDENCIES="gettext gnupg libcurl4-openssl-dev libfreetype6-dev libicu-dev libjpeg62-turbo-dev \
  libldap2-dev libmariadbclient-dev libmemcached-dev libpng-dev libpq-dev libxml2-dev libxslt-dev \
  unixodbc-dev uuid-dev"

POSTGRES_DEPENCIES="libpq5"

RUNTIME_DEPENDENCIES="ghostscript libaio1 libcurl4 libgss3 libicu63 libmcrypt-dev libxml2 libxslt1.1 \
  libzip-dev locales sassc unixodbc unzip zip"

MEMCACHED_DEPENDENCIES="libmemcached11 libmemcachedutil2"

apt-get update
apt-get install -y --no-install-recommends apt-transport-https \
    $BUILD_DEPENDENCIES \
    $POSTGRES_DEPENCIES \
    $RUNTIME_DEPENDENCIES \
    $MEMCACHED_DEPENDENCIES

echo 'Generating locales..'
echo 'en_US.UTF-8 UTF-8' > /etc/locale.gen
#echo 'fr_US.UTF-8 UTF-8' > /etc/locale.gen
locale-gen

echo "Installing php extensions"

# ZIP
docker-php-ext-configure zip --with-zip
docker-php-ext-install zip

docker-php-ext-install -j$(nproc) \
    intl \
    mysqli \
    opcache \
    pgsql \
    soap \
    xsl

# GD.
docker-php-ext-configure gd --with-freetype=/usr/include/ --with-jpeg=/usr/include/
docker-php-ext-install -j$(nproc) gd

# # LDAP.
# docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/
# docker-php-ext-install -j$(nproc) ldap

# APCu, igbinary, Memcached, MongoDB, Redis, Solr, uuid, XMLRPC
pecl install apcu igbinary memcached solr uuid 

#pecl install channel://pecl.php.net/xmlrpc-1.0.0RC3  xmlrpc => indisponible en version 7.4 mais en 8.0

apt-get update -y && apt-get upgrade -y;
apt-get install -y libxml2-dev
docker-php-ext-install -j$(nproc) xmlrpc

docker-php-ext-enable apcu igbinary memcached solr uuid xmlrpc

echo 'apc.enable_cli = On' >> /usr/local/etc/php/conf.d/docker-php-ext-apcu.ini

# Keep our image size down..
pecl clear-cache
apt-get remove --purge -y $BUILD_PACKAGES
apt-get autoremove -y
apt-get clean
rm -rf /var/lib/apt/lists/*