#!/bin/bash

export $(grep -v '^#' /tmp/.env | xargs -d '\n')

# Doc: https://docs.moodle.org/4x/fr/Administration_en_ligne_de_commande

# Moodle installation
if [ ! -f /var/www/html/moodle/config.php ]; then
  echo "Install Moodle"
  # echo "php /var/www/html/moodle/admin/cli/install.php --chmod=0777 --dataroot=/var/www/moodledata --lang=$MDL_LANG --wwwroot=http://192.168.42.158 --dbtype=$MDL_DB_TYPE --dbhost=$MDL_DB_HOST --dbname=$MDL_DB_NAME --dbuser=$MDL_DB_USER --dbpass=$MDL_DB_PASS --dbport=$MDL_DB_PORT --adminuser=$MDL_ADMIN_NAME --adminpass=$MDL_ADMIN_PASS --agree-license --non-interactive --allow-unstable --fullname=$MDL_FULL_NAME --shortname=$MDL_SHORT_NAME --skip-database"
  # su - www-data -s /bin/bash -c "php /var/www/html/moodle/admin/cli/install.php --chmod=0777 --dataroot=/var/www/moodledata --lang=$MDL_LANG --wwwroot=http://192.168.42.158 --dbtype=$MDL_DB_TYPE --dbhost=$MDL_DB_HOST --dbname=$MDL_DB_NAME --dbuser=$MDL_DB_USER --dbpass=$MDL_DB_PASS --dbport=$MDL_DB_PORT --adminuser=$MDL_ADMIN_NAME --adminpass=$MDL_ADMIN_PASS --agree-license --non-interactive --allow-unstable --fullname=$MDL_FULL_NAME --shortname=$MDL_SHORT_NAME --skip-database"
  #Fonctionne avec la valeur de base 
  echo "php /var/www/html/moodle/admin/cli/install.php --chmod=0777 --dataroot=/var/www/moodledata --lang=$MDL_LANG --wwwroot=http://localhost --dbtype=$MDL_DB_TYPE --dbhost=$MDL_DB_HOST --dbname=$MDL_DB_NAME --dbuser=$MDL_DB_USER --dbpass=$MDL_DB_PASS --dbport=$MDL_DB_PORT --adminuser=$MDL_ADMIN_NAME --adminpass=$MDL_ADMIN_PASS --agree-license --non-interactive --allow-unstable --fullname=$MDL_FULL_NAME --shortname=$MDL_SHORT_NAME --skip-database"
  su - www-data -s /bin/bash -c "php /var/www/html/moodle/admin/cli/install.php --chmod=0777 --dataroot=/var/www/moodledata --lang=$MDL_LANG --wwwroot=http://localhost --dbtype=$MDL_DB_TYPE --dbhost=$MDL_DB_HOST --dbname=$MDL_DB_NAME --dbuser=$MDL_DB_USER --dbpass=$MDL_DB_PASS --dbport=$MDL_DB_PORT --adminuser=$MDL_ADMIN_NAME --adminpass=$MDL_ADMIN_PASS --agree-license --non-interactive --allow-unstable --fullname=$MDL_FULL_NAME --shortname=$MDL_SHORT_NAME --skip-database"
  sed -i '/0777;/i $CFG->sslproxy = true;' /var/www/html/moodle/config.php
fi

if [[ -f /var/www/html/moodle/config.php ]] && [[ $(cat /var/www/html/moodle/config.php | grep '#db_installed#' 2>/dev/null) == "" ]]; then
  echo "Install database..."
  su - www-data -s /bin/bash -c "php /var/www/html/moodle/admin/cli/install_database.php --agree-license --adminpass=$MDL_ADMIN_PASS"
  echo '#db_installed#' >>/var/www/html/moodle/config.php
fi

chown www-data:www-data /var/www/html/moodle/config.php
chmod 750 /var/www/html/moodle/config.php

echo "Setup cron..."
touch /var/log/cron.log
chown www-data:www-data /var/log/cron.log
su - www-data -s /bin/bash -c "(crontab -l 2>/dev/null; echo '*/1 * * * * /usr/local/bin/php /var/www/html/moodle/admin/cli/cron.php > /var/log/cron.log') | crontab -"
service cron restart

#Xdebug installation
echo "Setup Xdebug..."
if [ ! -f /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini ]; then
  apt update
  apt install -y iproute2
  pecl install xdebug-3.0.4
  docker-php-ext-enable xdebug
  echo 'xdebug.remote_port=9003' >>/usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
  echo 'xdebug.mode=debug' >>/usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
  echo 'xdebug.start_with_request=1' >>/usr/local/etc/php/conf.d/docker-php-ext-lxdebug.ini
  echo 'xdebug.client_host='$(/sbin/ip route | awk '/default/ { print $3 }') >>/usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
  echo 'xdebug.idekey=PHPSTORM' >>/usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
fi

#Plugins installation
if [ $MDL_VERSION = "39" ];then
  echo "Installing trax log plugin"
  if ! sudo test -d /var/www/html/moodle/admin/tool/log/store/trax/ ; then
    sudo curl -Lo /tmp/trax.zip https://github.com/trax-project/moodle-trax-logs/archive/refs/tags/v0.20.zip
    sudo unzip -q /tmp/trax.zip -d /tmp/
    sudo rm /tmp/trax.zip
    mkdir -p /var/www/html/moodle/admin/tool/log/store/trax/
    sudo mv /tmp/moodle-trax-logs-0.20 /var/www/html/moodle/admin/tool/log/store/trax/
    sudo chown -R www-data:www-data /var/www/html/moodle/admin/tool/log/store/trax/

    # Trax insertion completion
    echo "INSERT INTO public.mdl_config_plugins (plugin, name, value) VALUES ('logstore_trax', 'version', '2020060900');" >> /tmp/trax_plugin_installation.sql
    echo "INSERT INTO public.mdl_config_plugins (plugin, name, value) VALUES ('logstore_trax', 'platform_iri', 'http://moodle');" >> /tmp/trax_plugin_installation.sql
    echo "INSERT INTO public.mdl_config_plugins (plugin, name, value) VALUES ('logstore_trax', 'lrs_endpoint', 'http://trax/trax/api/$ENDPOINT_UUID/xapi/std/');" >> /tmp/trax_plugin_installation.sql
    echo "INSERT INTO public.mdl_config_plugins (plugin, name, value) VALUES ('logstore_trax', 'lrs_username', 'moodle');" >> /tmp/trax_plugin_installation.sql
    echo "INSERT INTO public.mdl_config_plugins (plugin, name, value) VALUES ('logstore_trax', 'lrs_password', '$ENDPOINT_PASSWORD');" /tmp/trax_plugin_installation.sql
  
    PGPASSWORD=$(echo $MDL_DB_PASS) psql -h $MDL_DB_HOST -U $MDL_DB_USER -d $MDL_DB_NAME -a -f /tmp/trax_plugin_installation.sql
    sudo rm /tmp/trax_plugin_installation.sql

    # Trax logs plugin activation
    PGPASSWORD=$(echo $MDL_DB_PASS) psql -h $MDL_DB_HOST -U $MDL_DB_USER -d $MDL_DB_NAME -c "update mdl_config_plugins set value ='logstore_standard,logstore_trax' where name = 'enabled_stores'; WITH config_log AS( insert into mdl_config_log (userid, timemodified, name, oldvalue, value, plugin) VALUES (2, extract(epoch from now()), 'tool_logstore_visibility', 0, 1, 'logstore_trax') RETURNING id) INSERT INTO mdl_logstore_standard_log (eventname, component, action, target, objecttable, objectid, crud, edulevel, contextid, contextlevel, contextinstanceid, userid, courseid, relateduserid, anonymous, other, timecreated, origin, ip, realuserid) VALUES ('\core\event\config_log_created', 'core', 'created', 'config_log', 'config_log', (SELECT id from config_log), 'c', '0', '1', '10', '0', '2', '0', NULL, '0', '{\"name\":\"tool_logstore_visibility\",\"oldvalue\":\"0\",\"value\":\"1\",\"plugin\":\"logstore_trax\"}', extract(epoch from now()), 'web', '127.0.0.1', NULL) ;"
  else
      echo "Trax log plugin already installed"
  fi

  echo "Installing trax video plugin"
  if sudo test ! -d /var/www/html/moodle/mod/traxvideo ; then
    sudo curl -Lo /tmp/traxvideo.zip https://github.com/trax-project/moodle-trax-video/archive/refs/tags/v0.4.zip
    sudo unzip -q /tmp/traxvideo.zip -d /var/www/html/moodle/mod/
    sudo rm /tmp/traxvideo.zip
    mkdir -p /var/www/html/moodle/mod/traxvideo
    sudo mv /var/www/html/moodle/mod/moodle-trax-video-0.4 /var/www/html/moodle/mod/traxvideo/
    sudo chown -R www-data:www-data /var/www/html/moodle/mod/traxvideo/
  else
    echo "Trax video plugin already installed"
  fi

else
  echo "No plugins to install"
fi